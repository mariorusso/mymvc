<?php

namespace issd\controllers; //namespace declaration; mean that this interface is a part of issd\controllers.

use issd\system\Controller; //use make sure that the issd\system\Controller is used.

/* Page controller extends Controller and
 * have methods that determine which page in the website.
 */
class PageController extends Controller
{
    //Constructor accepts 2 parameters, $methods and $params. 		
    public function __construct($method, $params)
    {
       parent::__construct($method, $params);
    }
    
	//index method, is the basic method to define the index page. BUT now the one been used is from the main controller. 
    public function index()
    {
    	$data['title'] = 'Welcome!!!';
		$data['content'] = '<p>Welcome to my index page...</p>';
		$this->loadview('page', $data);   
    }
    
	/* about method is the method used to create the about page.
	* it define the title banner and the name of the content to be load.
	* the next method have the same function for each page.
    */
    public function about()
    {
    	$data['title'] = 'ABOUT US';
		$data['banner'] = '<img src=/assets/images/about_banner.jpg>';
		$data['content'] = 'about';
		$this->loadview('page', $data);     	
    }
	
	public function support()
    {
    	$data['title'] = 'SUPPORT';
		$data['banner'] = '<img src=/assets/images/support_banner.jpg>';
		$data['content'] = 'support';
		$this->loadview('page', $data);
     	   
    }
	
	public function contact()
    {
    	$data['title'] = 'CONACT';
		$data['content'] = '<p>If you need CONTACT us we are very friendly...</p>';
		$this->loadview('page', $data);
     	   
    }
	
	public function sales()
    {
    	$data['title'] = 'SALES';
		$data['content'] = '<p>Contact our sales team.... We have the right product for you...</p>';
		$this->loadview('page', $data);
     	   
    }
	
	
}