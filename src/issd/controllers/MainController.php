<?php

namespace issd\controllers;

use issd\system\Controller;

class MainController extends Controller
{
    public function __construct($method, $params)
    {
        parent::__construct($method, $params);
    }
    
    public function index()
    {
     	$data['title'] = 'Welcome!!!';
		$data['calltoaction'] = '<p>Welcome to my index page...</p>';
		$data['banner'] = '<img src=/assets/images/home_banner.jpg>';
		$this->loadview('home', $data);  
    }
    
  
}