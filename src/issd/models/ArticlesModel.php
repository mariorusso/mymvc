<?php

namespace issd\models;
use issd\system\Model;

/*Articles Model*/

class ArticlesModel extends Model{
	
	protected $dbh;
	
	public function __construct() 
	{		
		parent::__construct();
		
	}
	
	public function getAll()
	{
		$query =  'SELECT * FROM articles';
		$statement = $this->dbh->prepare($query);
		$statement->execute();
		return $statement->fetchall(\PDO::FETCH_ASSOC);
	}
	
	public function getOne($id)
	{
		$query =  'SELECT * FROM articles where id = :id';
		$params = [':id' => $id];
		$statement = $this->dbh->prepare($query);
		$statement->execute($params);
		return $statement->fetch(\PDO::FETCH_ASSOC);
	}
	
	public function save($data)
	{
		$query =  'INSERT INTO articles (title, body, created_at) VALUES (:title , :body, NOW())';
		$params = [':title' => $data['title'], ':body' => $data['body']];
		$statement = $this->dbh->prepare($query);
		$statement->execute($params);
		$statement->fetch(\PDO::FETCH_ASSOC);
		return $this->dbh->lastInsertId();
	}
	
	public function update($data)
	{				
		$query =  'UPDATE articles SET title = :title, body = :body WHERE id = :id';
		$params = [':title' => $data['title'], ':body' => $data['body'], ':id' => $data['id']];
		$statement = $this->dbh->prepare($query);
		$statement->execute($params);
		return $statement->rowCount();
	}
}


