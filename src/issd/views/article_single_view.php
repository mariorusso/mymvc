<?php $this->loadPartial('header', $data); ?>
<!-- content -->
<div class="container">

  <div class="row">
    <div id="site-tagline" class="col-xs-12">
      <h1><?=esc($title)?></h1>
    </div>
  </div>
  
  <!-- Article content -->
  <div class="row">
        <div class="col-xs-12 col-sm-4">
        	<?=$img?>
        </div>
    	<div class="col-xs-12 col-sm-8 article">
			<p class="date"><?=esc($article['created_at'])?></p>
			<p><?=esc($article['body'], true)?></p>			
			<p><a class="btn btn-default" href="<?=BASE_URL?>/articles/edit/<?=$article['id']?>"> EDIT </a></p>
			
		</div>
		   
        
  </div><!-- END article -->
    
</div><!-- /content -->

<?php $this->loadPartial('footer', $data); ?>