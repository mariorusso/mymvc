<?php

namespace issd\controllers; //namespace declaration; mean that this interface is a part of issd\controllers.

use issd\system\Controller; //use make sure that the issd\system\Controller is used.
use issd\models\ArticlesModel; //use make sure that the issd\models\ArticlesModel is used.

/* ArticlesController extends Controller and
 * have methods that determine the articles related pages.  
 */
class ArticlesController extends Controller
{
	
    public function __construct($method, $params)
    {
    	$this->model = new ArticlesModel();
		parent::__construct($method, $params);
		
    }
	
	
    /* index method is the method used to create the list of articles page.
	* it call the getAll functions in the model...
	* it define the title banner and image to be loaded.
    */
    public function index()
    {
    	$data['articles'] = $this->model->getAll();
		$data['title'] = 'All our articles';
		$data['banner'] = '<img src=/assets/images/articles_banner.jpg>';
		$data['img'] = '<img src=/assets/images/in_about.jpg>';
		$this->loadView('article', $data);
    }
    
	/* show method is the method used to create each article page. 
	* Receive the article ID as a parameter.
	* it call the getOne functions in the model...
	* it define the title banner and image to be loaded.
	* call the loadView method passing the page name(string) and the $data(array).
    */
	public function show($id)
	{
		$data['article'] = $this->model->getOne($id);
		$data['title'] = $data['article']['title'];
		$data['img'] = '<img src=/assets/images/article' . $id . '.jpg>';
		$this->loadView('article_single', $data);
	}
	/* create method is used to create the page create article page
	 * sets the title and call the loadView method passing the page name(string) and the $data (array). 
	 */
	public function create()
	{
		$data['title'] = 'Insert new articles';
		$this->loadView('article_create', $data);
	}
	
	/* add method is called after the create article form is submitted, 
	 * it checks if post is empty, 
	 * go through the validation from Valitron
	 * checks if validate and save it
	 * if does not validate show errors.  
	 */
	public function add() 
	{
		if(empty($_POST)) {
			$this->create();
		}
		$v = new \Valitron\Validator($_POST);
		$v->rule('required', 'title');
		$v->rule('required', 'body');
		if($v->validate()) {
				$id = $this->model->save($_POST);
			if($id){
				$this->show($id);
				exit;
			}
		}else{
			//Errors
			var_dump('DANG, WE HAVE ERRORS!!');
			var_dump($v->errors());
		}
		
	}
	
	/* edit method used to edit article in a form, 
	 * accepts a parameter, the article ID 
	 * uses the getOne function to retrieve that article from DB 
	 * call the loadView method passing the page name(string) and the $data (array). 
	 */
	public function edit($id)
	{
		$data['article'] = $this->model->getOne($id);
		$data['title'] = $data['article']['title'];
		$data['body'] = $data['article']['body'];
		$this->loadView('article_edit', $data);
	}
	
	/* update method is called after the edit article form is submitted, 
	 * it checks if post is empty, 
	 * go through the validation from Valitron
	 * checks if validate and save it
	 * if does not validate show errors.  
	 */
	public function update() 
	{
		if(empty($_POST)) {
			$this->index();
			exit;
		}
		$v = new \Valitron\Validator($_POST);
		$v->rule('required', 'id');
		$v->rule('required', 'title');
		$v->rule('required', 'body');
		if($v->validate()) {
			
			$id = $this->model->update($_POST);
			
			if($id){
				$this->show($_POST['id']);
				exit;
			}else {
				$this->create();
				exit;
			}
			die;
		}else{
			//Errors
			var_dump('DANG, WE HAVE ERRORS!!');
			var_dump($v->errors());
		}
		
	}
  
}