<?php

namespace issd\system;
use issd\system\FileLogger;
use issd\system\DatabaseLogger;
 
require 'src/issd/helpers/helper_functions.php';
require 'src/issd/config/config.php';
require 'src/issd/config/database.php';
require 'src/issd/config/paths.php';

class Bootstrap 
{

    public function __construct()
    {
        $route = new Router;
		$logger = new FileLogger();
		$logger = new DatabaseLogger();
        $loader = new Loader($route, $logger);
		
    }


}