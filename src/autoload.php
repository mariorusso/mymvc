<?php
//Magic function
function __autoload($class_to_load)
{

    $class_to_load = str_replace('\\', '/', $class_to_load);
    
 //get parent directory of THIS file
 $src_dir = dirname(__FILE__);
    
 //concatonate the filename we want to include
 $class_name = $src_dir. DIRECTORY_SEPARATOR . $class_to_load .'.php';

    
 // Require the classes
 require ($class_name);
    
}