<?php $this->loadPartial('header', $data); ?>
<!-- content -->
<div class="container">

  <div class="row">
    <div class="col-xs-12">
      <h1><?=esc($title, true)?></h1>
    </div>
  </div>
  
  <!-- Edit article form -->
  <div class="row">
    
			<div class="col-xs-12">
				<form class="form-horizontal" role="form" action="<?=BASE_URL?>/articles/add" method="post">
				
					<input type="hidden" name="id" id="id" />
					<div class="form-group">
    					<label class="control-label col-sm-2"  for="title">Title:</label>
    					<div class="col-sm-10">
    						<input type="text" name="title" id="title" />
    					</div>
  					</div>
  					<div class="form-group">
    					<label class="control-label col-sm-2" for="body">Body:</label>
    					<div class="col-sm-10">
    						<textarea name="body" id="body" ></textarea>
    					</div>
  					</div>
					<button type="submit" class="btn btn-default submit">Submit</button>
					
				</form>
		
			</div>
      
  </div><!-- END of form -->
    
</div><!-- /content -->

<?php $this->loadPartial('footer', $data); ?>