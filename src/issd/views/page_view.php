<!-- Load the Header using PHP function -->	
<?php $this->loadPartial('header', $data); ?>

<!-- Page top Below Header Nav -->
<div class="container-fluid">
	
	<!-- TOP BANNER -->
	<div class="row">
		
		<div class="col-xs-12 banner">
      	<?=$banner?>
      	
      	<h1><?=$title?></h1>
      	
      </div>
	</div><!-- END of TOP BANNER-->
	
</div><!-- END of Page top -->
	
<!-- Main Content -->
<div class="container">
 
  <!-- Page Content -->
  <div class="row">
    	   
    	   <!-- Load the Content using PHP function -->	
    	   <?php $this->importContent($content); ?>
        
  </div><!-- END Page Content -->

</div><!-- END of Main Content -->

<!-- Load the footer using PHP function -->		
<?php $this->loadPartial('footer', $data); ?>