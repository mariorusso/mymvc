<?php

namespace issd\system;

use issd\system\interfaces\Logger;

class Loader
{
    private $logger;
	
    public function __construct(Router $route, Logger $logger)
    {
       $this->logger = $logger;    
       $this->LoadController($route);           
    }
        
    
    private function LoadController(Router $route)
    {
        
     	if($route->getController())
        {
            $controller = '\issd\controllers\\' . ucfirst($route->getController() . 'Controller');
        } else {
         $controller = '\issd\controllers\MainController';   
        }
        
        if($this->fileExists($controller))
        {
         	$this->logger->log($route->getController() . '/' . $route->getMethod() . '/' . $route->getParams() . 'code: 200');
            new $controller($route->getMethod(), $route->getParams());   
        } else {
          	$this->logger->log($route->getController() . '/' . $route->getMethod() . '/' . $route->getParams() . 'code: 404');
            new \issd\controllers\MainController('Error404', null);
        }    
    }
   
      
    private function fileExists($controller)
    {
     $file = str_replace('\\', DIRECTORY_SEPARATOR, $controller) . '.php';
        $file = SRC_PATH . $file;
             
        if(file_exists($file))
        {
            return true;
        } else {
            return false;
        }
    }
    
}