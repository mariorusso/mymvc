<?php

ob_start();
session_start();

/* BASIC SITE CONFIG*/

define('SITE_NAME', 'My MVC Framework');
define('BASE_URL', 'http://mvc.mr/index.php/?');

date_default_timezone_set('America/Winnipeg');
