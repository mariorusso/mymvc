<?php

namespace issd\system\interfaces;

interface Logger{
	
	public function log($message);
}
