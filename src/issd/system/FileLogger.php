<?php

namespace issd\system;

use issd\system\interfaces\Logger;

class FileLogger implements Logger {
	
	private $logfile;
	
	public function __construct()
	{
		$this->logfile = fopen(LOG_PATH . 'access_log.txt', "a+");
	}
	
	public function log($message)
	{
		$date =  date('Y-m-d h:m:s');
		$ip = esc($_SERVER['REMOTE_ADDR']);
		$logfile = $date . ', ' . $ip . ', ' . $message . "\r\n";
		fwrite($this->logfile, $logfile);
	}
	
	public function __destruct()
	{
		fclose($this->logfile);
	}
	
}
