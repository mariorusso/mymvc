<?php

namespace issd\system;

use issd\system\interfaces\Logger;

class DatabaseLogger extends Model implements Logger {
	
	private $logtable;
	
	public function __construct()
	{
		$this->logtable = 'access_log';
		parent::__construct();
	}
	
	public function log($message)
	{
		$date =  date('Y-m-d h:m:s');
		$ip = esc($_SERVER['REMOTE_ADDR']);
		$logline = $date . ', ' . $ip . ', ' . $message . "\r\n";
		$this->insertLogLine($logline);		
	}
	
	private function insertLogLine($logline)
	{
		$query = 'INSERT INTO access_log (logline) VALUES (:logline)';
		$params = ['logline' => $logline];
		$statement = $this->dbh->prepare($query);
		$statement->execute($params);		
	}
	
	
}
