<!DOCTYPE >

<html>
	<head>
		<title><?=$title?> | <?=SITE_NAME?></title>
		<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css" rel="stylesheet" />
		<link href="/assets/css/style.css" rel="stylesheet" />
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
		<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
	</head>
	<body>
		<div class="container-fluid">

		  <div class="row">
		  	
		    <nav id="topnav" class="navbar navbar-inverse">
		      <div class="navbar-header">
		      	 
		      	<button type="button" class="navbar-toggle" data-toggle="collapse" 
		        		data-target="#my-navbar-collapse">
		         	<span class="icon-bar"></span>
		         	<span class="icon-bar"></span>
		         	<span class="icon-bar"></span>
		      	</button>
		      
				<a class="navbar-brand">
					<img src="/assets/images/logo1.png" alt="MR MVC LOGO" />
		        </a>
		     </div>
			     <div class="collapse navbar-collapse" id="my-navbar-collapse">
			      	<ul id="menu-navbar" class="nav navbar-nav">
			      		<li><a href="<?=BASE_URL?>">Home</a></li>
						<li><a href="<?=BASE_URL?>/articles">Articles</a></li>
						<li><a href="<?=BASE_URL?>/page/about">About</a></li>
						<li><a href="<?=BASE_URL?>/page/support">Support</a></li>
						<li><a href="<?=BASE_URL?>/articles/create">Create</a></li>
					</ul>     
				</div>
		    </nav>
		
		  </div>
		
		</div><!-- /Navbar -->
		