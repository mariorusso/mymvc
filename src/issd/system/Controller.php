<?php

namespace issd\system;

class Controller
{
	private $srcpath;
    
    public function __construct($method, $params)
    {
        
        if(!is_null($method) && !empty($method))
        {
        	if(method_exists($this, $method)){
        		$this->$method($params);
        	} else {
        		$this->Error404();
	 		}
         	
        } else {
         	$this->index();   
        }//endif
        
    }//end construct
    
    public function Error404() 
    {
     	$data['title'] = '404 Error - Page not found';
		$data['content'] = '<p>Sorry, but we can\'t seem to find the content you are looking for.</p>';
     	$this->loadView('page', $data); 
    }
	
	/* loadView method accepts 2 parameters the $view and the data array
	 * uses the view to find the file and define it in a $file variable 
	 * extract the data array and include the $file	 * 
	 */
	public function loadView($view, $data = [])
	{
		$file = VIEW_PATH . $view . '_view.php';
		extract($data);
		include($file);
	}
	
	public function importContent($page,  $data = [])
	{
		$file = HTML_PATH . $page . '.php';
		extract($data);
		include($file);
	}
	
	public function loadPartial($view, $data = [])
	{
		$file = VIEW_PATH . $view . '.php';
		extract($data);
		include($file);
	}
    
    
}//end controller