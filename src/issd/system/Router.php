<?php

namespace issd\system;

class Router
{
 
    private $query;
    private $controller;
    private $method;
    private $params;
    
    public function __construct()
    {
        
        $this->setQuery();
        $this->setController();
        $this->setMethod();
        $this->setParams();
    }
    
        
    //------------------------setters--------------------------//
    
    public function setQuery()
    {
     
       $this->query = explode('/',$_SERVER['QUERY_STRING']);
        
        if(empty($this->query[0]))
        {
         array_shift($this->query);   
        }
        
    }
    
    
    public function setController()
    {
        if($this->query)
        {
         $this->controller = array_shift($this->query);   
                
        }
        
    }
    
    
    
     public function setMethod()
    {
        if($this->query)
        {
         $this->method = array_shift($this->query);   
                
        }   
    }
    
    
    
    public function setParams()
    {
        if($this->query)
        {
         $this->params = array_shift($this->query);   
                    
        }    
    }
    
    //------------------------getters--------------------------//
    
    public function getController()
    {
        return $this->controller;   
    }
    public function getMethod()
    {
        return $this->method;   
    }
    public function getParams()
    {
        return $this->params;   
    }
    
    
}