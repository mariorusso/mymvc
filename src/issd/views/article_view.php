<!-- Load the Header using PHP function -->
<?php $this->loadPartial('header', $data); ?>
<!-- Page top Below Header Nav -->
<div class="container-fluid">
	
	<!-- TOP BANNER -->
	<div class="row">
		
		<div class="col-xs-12 banner">
      	<?=$banner?>
      	
      	<h1><?=$title?></h1>
      	
      </div>
	</div><!-- END of TOP BANNER-->
	
</div><!-- END of Page top -->

 <!-- Main Content -->
<div class="container">

  <div class="row">
    <div class="col-xs-12">
      <h1><?=$title?></h1>
    </div>
  </div>
  
  <!-- list of articles -->
  <div class="row">
        
        <!-- Loop through all articles to display them -->
    	<?php foreach ($articles as $article) : ?>
			<div class="col-xs-12 col-sm-4">
				
				<!-- Display article title --> 
				<h3 class="art_title">
					<?=esc($article['title'])?>
				</h3>
				
				<!-- Display article date --> 
				<p class="date">
					<?=esc($article['created_at'])?>
				</p>
				
				<!-- Display the article link --> 
				<p>
					<a href="<?=BASE_URL?>/articles/show/<?=$article['id']?>" class="btn btn-default">
						Read more &gt;&gt;
					</a>
				</p>
				
			</div>
		<?php endforeach ?><!-- END of loop -->
        
  </div><!-- END of list of articles -->
    
</div><!-- END of Main Content -->

<!-- Load the footer using PHP function -->	
<?php $this->loadPartial('footer', $data); ?>