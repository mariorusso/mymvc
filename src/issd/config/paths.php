<?php

define('SRC_PATH', dirname(dirname(__DIR__)) . DIRECTORY_SEPARATOR);
define('VIEW_PATH', SRC_PATH . 'issd' . DIRECTORY_SEPARATOR . 'views' . DIRECTORY_SEPARATOR);
define('HTML_PATH',  dirname(dirname(dirname(__DIR__))) . DIRECTORY_SEPARATOR . 'assets' . DIRECTORY_SEPARATOR . 'content' . DIRECTORY_SEPARATOR);
define('LOG_PATH', SRC_PATH . 'issd' . DIRECTORY_SEPARATOR . 'log' . DIRECTORY_SEPARATOR);

